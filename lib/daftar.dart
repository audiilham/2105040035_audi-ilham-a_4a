import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'dart:ui';
import 'package:google_fonts/google_fonts.dart';
import 'package:myapp/utils.dart';
import 'package:myapp/home.dart';
import 'package:myapp/login.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController passwordValController = TextEditingController();

  bool errorValidator = false;

  Future signUp() async {
    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: emailController.text,
        password: passwordValController.text,
      );
      await FirebaseAuth.instance.signOut();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    double baseWidth = 410;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
            color: Color(0xff51c49b),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding:
                    EdgeInsets.fromLTRB(77 * fem, 95 * fem, 75 * fem, 66 * fem),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          0 * fem, 0 * fem, 5 * fem, 23 * fem),
                      child: Text(
                        'AirSol',
                        style: SafeGoogleFont(
                          'Aclonica',
                          fontSize: 32 * ffem,
                          fontWeight: FontWeight.w400,
                          height: 1.1325 * ffem / fem,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          0 * fem, 0 * fem, 0 * fem, 1 * fem),
                      child: Text(
                        'The Air Quality App Built ',
                        style: SafeGoogleFont(
                          'Quicksand',
                          fontSize: 20 * ffem,
                          fontWeight: FontWeight.w600,
                          height: 1.25 * ffem / fem,
                          color: Color(0xfff4f4f4),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          0 * fem, 0 * fem, 11 * fem, 0 * fem),
                      child: Text(
                        'For Jakarta',
                        style: SafeGoogleFont(
                          'Quicksand',
                          fontSize: 20 * ffem,
                          fontWeight: FontWeight.w600,
                          height: 1.25 * ffem / fem,
                          color: Color(0xfff4f4f4),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding:
                    EdgeInsets.fromLTRB(26 * fem, 50 * fem, 27 * fem, 84 * fem),
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Color(0xfff5f5f5),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40 * fem),
                    topRight: Radius.circular(40 * fem),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          0 * fem, 0 * fem, 1 * fem, 16 * fem),
                      child: Text(
                        'Daftar Sekarang',
                        style: SafeGoogleFont(
                          'Quicksand',
                          fontSize: 20 * ffem,
                          fontWeight: FontWeight.w700,
                          height: 1.25 * ffem / fem,
                          color: Color(0xff4d4d4d),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          65 * fem, 0 * fem, 62 * fem, 29 * fem),
                      width: double.infinity,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.fromLTRB(
                                0 * fem, 0 * fem, 2 * fem, 0 * fem),
                            child: RichText(
                              text: TextSpan(
                                style: SafeGoogleFont(
                                  'Quicksand',
                                  fontSize: 16 * ffem,
                                  fontWeight: FontWeight.w400,
                                  height: 1.25 * ffem / fem,
                                  color: Color(0xff000000),
                                ),
                                children: [
                                  TextSpan(
                                    text: 'Sudah punya Akun ?',
                                    style: SafeGoogleFont(
                                      'Quicksand',
                                      fontSize: 16 * ffem,
                                      fontWeight: FontWeight.w500,
                                      height: 1.25 * ffem / fem,
                                      color: Color(0xff444444),
                                    ),
                                  ),
                                  TextSpan(
                                    text: ' ',
                                  ),
                                ],
                              ),
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => LoginPage(),
                                ),
                              );
                            },
                            style: TextButton.styleFrom(
                              padding: EdgeInsets.zero,
                            ),
                            child: Text(
                              'Masuk',
                              style: SafeGoogleFont(
                                'Quicksand',
                                fontSize: 16 * ffem,
                                fontWeight: FontWeight.w700,
                                height: 1.25 * ffem / fem,
                                color: Color(0xff51c49b),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          0 * fem, 0 * fem, 235 * fem, 6 * fem),
                      child: Text(
                        'Username',
                        style: SafeGoogleFont(
                          'Quicksand',
                          fontSize: 19 * ffem,
                          fontWeight: FontWeight.w500,
                          height: 0.5 * ffem / fem,
                          color: Color(0xff444444),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          12 * fem, 0 * fem, 12 * fem, 8 * fem),
                      child: TextField(
                        controller: emailController,
                        decoration: InputDecoration(
                          hintText: 'Masukkan Username',
                          prefixIcon: Icon(
                            Icons.account_circle,
                            size: 30 * fem, //
                            color: Color(0xff51c49b),
                          ),
                          hintStyle: SafeGoogleFont(
                            'Quicksand',
                            fontSize: 18 * ffem,
                            fontWeight: FontWeight.w500,
                            height: 1.25 * ffem / fem,
                            color: Color.fromRGBO(175, 175, 175, 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10 * fem),
                            borderSide: BorderSide(
                              color: Color(0xffcfcfcf),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10 * fem),
                            borderSide: BorderSide(
                              color: Color(0xff51c49b),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          0 * fem, 0 * fem, 240 * fem, 6 * fem),
                      child: Text(
                        'Password',
                        style: SafeGoogleFont(
                          'Quicksand',
                          fontSize: 19 * ffem,
                          fontWeight: FontWeight.w500,
                          height: 1.25 * ffem / fem,
                          color: Color(0xff444444),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          12 * fem, 0 * fem, 12 * fem, 12 * fem),
                      child: TextField(
                        controller: passwordController,
                        obscureText: true,
                        decoration: InputDecoration(
                          prefixIcon: Icon(
                            Icons.lock,
                            size: 30 * fem, // ubah nilai size sesuai kebutuhan
                            color: Color(0xff51c49b),
                          ),
                          hintText: 'Masukkan Password',
                          errorText:
                              errorValidator ? "Password tidak sama!" : null,
                          hintStyle: SafeGoogleFont(
                            'Quicksand',
                            fontSize: 18 * ffem,
                            fontWeight: FontWeight.w500,
                            height: 1.25 * ffem / fem,
                            color: Color(0xffafafaf),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10 * fem),
                            borderSide: BorderSide(
                              color: Color(0xffcfcfcf),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10 * fem),
                            borderSide: BorderSide(
                              color: Color(0xff51c49b),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          0 * fem, 0 * fem, 165 * fem, 6 * fem),
                      child: Text(
                        'Validasi Password',
                        style: SafeGoogleFont(
                          'Quicksand',
                          fontSize: 19 * ffem,
                          fontWeight: FontWeight.w500,
                          height: 1.25 * ffem / fem,
                          color: Color(0xff444444),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          12 * fem, 0 * fem, 12 * fem, 23 * fem),
                      child: TextField(
                        controller: passwordValController,
                        obscureText: true,
                        decoration: InputDecoration(
                          prefixIcon: Icon(
                            Icons.lock,
                            size: 30 * fem, // ubah nilai size sesuai kebutuhan
                            color: Color(0xff51c49b),
                          ),
                          hintText: 'Masukkan Password',
                          errorText:
                              errorValidator ? "Password tidak sama!" : null,
                          hintStyle: SafeGoogleFont(
                            'Quicksand',
                            fontSize: 18 * ffem,
                            fontWeight: FontWeight.w500,
                            height: 1.25 * ffem / fem,
                            color: Color(0xffafafaf),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10 * fem),
                            borderSide: BorderSide(
                              color: Color(0xffcfcfcf),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10 * fem),
                            borderSide: BorderSide(
                              color: Color(0xff51c49b),
                            ),
                          ),
                        ),
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        setState(() {
                          passwordController.text == passwordValController.text
                              ? errorValidator = false
                              : errorValidator = true;
                        });
                        if (errorValidator) {
                          print("Error");
                        } else {
                          signUp();
                          Navigator.pop(context);
                        }
                      },
                      style: TextButton.styleFrom(
                        padding: EdgeInsets.zero,
                      ),
                      child: Container(
                        width: double.infinity,
                        height: 58 * fem,
                        decoration: BoxDecoration(
                          color: Color(0xff51c49b),
                          borderRadius: BorderRadius.circular(30 * fem),
                        ),
                        child: Center(
                          child: Text(
                            'Daftar',
                            style: SafeGoogleFont(
                              'Quicksand',
                              fontSize: 20 * ffem,
                              fontWeight: FontWeight.w700,
                              height: 1.25 * ffem / fem,
                              color: Color(0xffffffff),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
